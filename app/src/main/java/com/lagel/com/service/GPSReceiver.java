package com.lagel.com.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class GPSReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            Intent bkgService = new Intent(context, GPSService.class);
            context.startService(bkgService);
        }
        catch (Exception e){}
    }
}
